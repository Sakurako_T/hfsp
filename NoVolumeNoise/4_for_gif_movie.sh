#!/usr/bin/bash

########################################
# Please put the parameter file name 
# without .txt
fname="params_sample"
#fname=""
########################################

echo Preparing a gif movie.
python3 ./make_a_gif.py <<EOF
$fname
EOF

echo please check a gif file in ./mov directory
