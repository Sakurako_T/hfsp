#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import glob
import re
import os
import time
#import psutil



def is_float(s):
  try:
    float(s)
  except:
    return False
  return True

def readSystemSize(filename):
    with open(filename, "r") as f:
        for n in range(20):
            line = f.readline()#;print(line)
            if n==8:
                params= line[:-1].split(' ')
                xmax=params[2]
            if n==11:
                params= line[:-1].split(' ')
                imax=params[2]
            if n==17:
                params= line[:-1].split(' ')
                dt=params[2]   
    xmax = int(xmax)
    imax = int(imax)
    dt   = float(dt)
    return xmax,imax,dt





class VisualizeCellsAndLumen:
    def __init__(self):
        self.tc=0
        
        self.bottom=0
        self.top   =0
        self.left  =0
        self.right =0
        
        
    def readParams(self,f):
        line = f.readline()
        params= line[:-1].split('\t')
        del line
        
        xi_str = params[-7]
        xi     = float(xi_str[3:])
        tc_str = params[-2]
        tc     = float(tc_str[3:])
        vd_str = params[-3]
        vd     = float(vd_str[3:])
        rho_str = params[-9]
        rho     = float(rho_str[5:])
        rhoe_str = params[-8]
        #print(rhoe_str)
        rhoe     = float(rhoe_str[5:])

        return round(tc), xi, round(vd, 2)#round(1.0*rhoe/rho)

    
    def findCellPositionAndTime(self, data_c):
        self.tc  =float(data_c[0])
        
        x_G = int( data_c[2])
        y_G = int( data_c[3])
        self.bottom = int(x_G-imax/2+imax)
        self.top    = int(x_G+imax/2+imax)
        self.left   = int(y_G-imax/2+imax)
        self.right  = int(y_G+imax/2+imax)
        
        
        
    def process_1by1(self,cellfile,outname,dir_path,nofl=0):
        start = time.time()
        
        #make dir
        new_dir_path_recursive=dir_path+"/imgs"
        os.makedirs(new_dir_path_recursive, exist_ok=True)
        
        lumenfile = cellfile.replace('cell','lumen') 
        #fl_ = open(lumenfile, "r")
        num_lines =12#len(fl_.readlines())
        #fl_.close()
        #print("   ... Length of this txt is", num_lines)

        with open(cellfile, "r") as fc, open(lumenfile,"r") as fl:
            tc, xi , vd= self.readParams(fc)
            #print("   ...  (tm,xi)=(",tc,",",xi,")")
            title = "($t_d$, $\\xi$, $V_d$)=("+str(tc)+", "+str(xi)+", "+str(vd)+")"

            line = fc.readline()

                    
                    
            #print('cell data',len(line.split('\t')),line.split('\t')[-10:])
            line = fl.readline()
            del line
            #print('lumen data',len(line.split('\t')),line.split('\t')[-10:])
            line = fl.readline()
            
            #line = fl.readline()
            #print('lumen data',len(line.split('\t')),line.split('\t')[-10:])



            line_c = fc.readline()
            item_checker = line_c.split('\t')
            itemset = []
            #print('cell data ',len(item_checker),item_checker[:10])
            #print(item_checker[imax*imax-10:])
            for item in item_checker :
                if is_float(item) == True:
                    itemset.append(float(item))
                   
            #print(line_c)
            data_c = np.array( itemset)
            #print(data_c[:10])
            self.findCellPositionAndTime(data_c)

            nline=0
            for line_l in fl:
                bg_lumen = np.zeros((xmax+2*imax,xmax+2*imax))
                bg_cells = np.zeros((xmax+2*imax,xmax+2*imax))
                
                item_checker = line_l.split('\t')
                #print(item_checker[:10])
                itemset = []
                if item_checker[0].replace('.', '').isdecimal()==False:
                    break
                for item in item_checker :
                    if is_float(item) == True:
                        itemset.append(float(item))
                
                    
                data_lu = np.array( itemset)
                tl      = float(data_lu[0])
                img_lu  = np.reshape(data_lu[8:xmax*xmax+8], (xmax,xmax) )
                img_1   = bg_lumen[imax:-imax, imax:-imax]
                bg_lumen[imax:-imax, imax:-imax] = img_1+img_lu

                while tl == self.tc:

                    img_c = np.reshape(data_c[8:], (xmax,xmax))#(imax,imax) )
                    img_0   = bg_cells[imax:-imax, imax:-imax]
                    bg_cells[imax:-imax, imax:-imax] = img_c#+img_c
                    #img_0  = bg_cells[self.bottom:self.top, self.left:self.right]
                    #bg_cells[self.bottom:self.top, self.left:self.right] = img_0+img_c
                    
                    line_c = fc.readline()
                    item_checker = line_c.split('\t')
                    itemset = []
                    for item in item_checker :
                        if is_float(item) == False:
                            break
                        else:
                            itemset.append(float(item))
                    data_c = np.array( [ float(item) for item in line_c.split('\t') ])
                    self.findCellPositionAndTime(data_c)
                ##print('nline = ',nline)
                nline+=1
                tdt =  int(self.tc/dt)
                if (nline <= num_lines-5) & (tdt%10 ==0 ):
                    fig,ax = plt.subplots(1, 1, figsize=(8, 8))
                    out = np.zeros((xmax+2*imax,xmax+2*imax,3))
                    #print(sum(sum(bg_cells>1)))
                    out[:,:,0]=(bg_cells*0.4 + bg_lumen*0.9)
                    out[:,:,1]=(bg_cells*0.5 + bg_lumen*0.9)
                    out[:,:,2]=(bg_cells*0.99 + bg_lumen*0.0)
                    
                    out = out/255
                    
                    out[out[:,:,0]>1] = 1
                    out[out[:,:,1]>1] = 1
                    out[out[:,:,2]>1] = 1
                    #im=ax.imshow(img_bg,cmap="GnBu")#,np.int32)
                    ax.imshow(out)
                    columns = [0]#np.arange(0,800,200)
                    left = ['']
                    ax.set_xticks(columns)
                    ax.set_xticklabels( left)
                    ax.set_yticks(columns)
                    ax.set_yticklabels( left)
                    
                    #ax.set_title(title)

                    
                    savename = new_dir_path_recursive+'/img_'+outname+"{:08d}".format(tdt)+'.png'
                    fig.savefig(savename, bbox_inches="tight")
                    
                    plt.clf()
                    plt.close(fig)
                    #fig.colorbar(im)
                    #break
                
                    break


        #print("end of this analysis")

if __name__ == "__main__":
    file_system_size ='./system_size.h'
    xmax,imax,dt= readSystemSize(file_system_size)

    filename = input()#max(list_of_files, key=os.path.getmtime)
    print("   Outputting snapshot:",filename)

    dat_name=re.findall('pa.*l', filename)
    outname = dat_name[0]
    dir_path = './'
 
        
    obg=VisualizeCellsAndLumen()
    obg.process_1by1(filename,outname,dir_path)

      

