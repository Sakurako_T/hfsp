/* ////////////////////////////////////////////////////////////

   file:   main.cpp 
   auther: Tanida
   date:   20200624
   ver:    1.1
	inital num of cell : 4  

   objective: 
	This code simulates an organoid system, replicating experimental 
    procedures. It establishes initial cell counts and configures 
    visualizations similar to the setup of a camera on a microscope. 
    The code is also responsible for managing the time evolution 
    of the system's state. Within the time step loop, it calculates 
    state changes for each cell and lumen. For instance, if a cell 
    is prepared for division, the code triggers the cell-division 
    function.

/////////////////////////////////////////////////////////////*/
#define _GLIBCXX_DEBUG
#include <iostream>
#include <fstream>
#include <string>
#include <ctime> 
#include <cstdlib>    
#include <math.h>
//#include <direct.h> // for windows

#include "cell.h"
#include "interaction_field.h"
#include "system_size.h"

using namespace std;

int    tstep;
double xi;
double eta;
double rho    ;
double lumenV ; 
double t_i    ;
double vd     ;
double t_cell_death;

std::string replaceOtherStr(std::string &replacedStr, std::string from, std::string to) {
    const unsigned int pos = replacedStr.find(from);
    const int len = from.length();
 
    if (pos == std::string::npos || from.empty()) {
        return replacedStr;
    }
 
    return replacedStr.replace(pos, len, to);
}

bool readConditionFile(std::string input_name){

	
	std::ifstream input(input_name);

	if(input.is_open()){
		std::string number1;
           	std::getline(input,number1);
	        tstep = atoi(number1.c_str()); 

		std::string number2;
           	std::getline(input,number2);
	        xi = atof(number2.c_str()); 

        std::string number3;
           	std::getline(input,number3); 
	        eta = atof(number3.c_str()); 

        std::string number4;
           	std::getline(input,number4); 
	        rho = atof(number4.c_str()); 

        std::string number5;
           	std::getline(input,number5); 
	        lumenV = atof(number5.c_str()); 

		std::string number6;
           	std::getline(input,number6);
	        t_i = atof(number6.c_str()); 

		std::string number7;
           	std::getline(input,number7); 
	        vd = atof(number7.c_str()); 
        


	}

}


int main(){
	cout<< "start main.cpp ..."<<endl;

        std::string input_name="test.txt";
        cin >> input_name;
        readConditionFile(input_name);

	std::string input_number;
	cin >> input_number;
	std::string underbar_number="_";
	std::string str_number=underbar_number+input_number;
	

	int save_as_text_every_sec =  5.0/_dt; // about 1.0/dt would be fine
    int visualize_every_sec    = 10.0/_dt; // about 1.0/dt would be fine
    //cout << visualize_every_sec << endl;
   
	std::srand( time(NULL) );

	double v_organoid;
	


	

	// Set a interaction field________________________________
	InteractionField *field;
	field = new InteractionField;
	field->setInitialInteractionFieldCondition();

	// Set a lumen field______________________________________
	Cell *lumen;
	lumen = new Cell(_XMAX,_YMAX,xi,eta,rho,lumenV,t_i,t_cell_death,-3); // to be sure to use _XMAX & _YMAX
	lumen->setInitialLumenCondition();

	// Put initial cells in the field_________________________
    int tot_num_cell = 4;
	int tot_num_cell_tmp = tot_num_cell;
	Cell **cell;
	cell = new Cell*[500];
	for( int m=0 ; m<tot_num_cell; m++){
		cell[m] = new Cell(_IMAX,_JMAX,xi,eta,rho,lumenV,t_i,t_cell_death,/*id*/m);
		cell[m]->setInitialCellCondition(_XMAX/2+50*(m%2),_YMAX/2+50*int(m/2),3);
		cell[m]->initializeCentrosome();	
	}
    int new_id = tot_num_cell;

	// Mapping of all cells__________________________________
	Cell *cell_field;
	cell_field = new Cell(_XMAX,_YMAX,xi,eta,rho,lumenV,t_i,t_cell_death,-2); 
    cell_field ->setInitialLumenCondition();

	//_______________________________________________________
	


    // preparing data stock /////////////////////////////////////////////////////////////////////
    //
		std::string str_head;
		str_head = replaceOtherStr(input_name, ".txt", str_number);
		std::string str_extention(".dat");
	//
    // Files #1~3 contain data for analysis. These can be translated into 
    // 2D intensity images generated at every timestep as defined in params_***.txt.
    //
	/*1*/std::string str_lumen("_lumen");
	     std::string filename_lumen = /*str_dir+*/str_head+ str_lumen +str_extention;
	     std::ofstream outputlumen( filename_lumen );
	     lumen->writeHeader(outputlumen,_XMAX,_YMAX,vd,t_i,t_cell_death); //(x,y)
	/*2*/std::string str_cell("_cell");
	     std::string filename_cell  = /*str_dir+*/str_head+ str_cell  +str_extention;
	     std::ofstream outputfile( filename_cell );
	     cell_field->writeHeader(outputfile,_XMAX,_YMAX,vd,t_i,t_cell_death); //(i,j)
	/*3*/std::string str_phi("_phi");
	     std::string filename_phi  = /*str_dir+*/str_head+ str_phi  +str_extention;
	     std::ofstream outputphi( filename_phi );
	     field->writeHeader(outputphi); //(x,y)
    //
    //------------------------------------------------------------------------------
    // Files #4 & #5 were utilized for bug checks regarding the positions of 
    // centrosomes during cell division and the distribution of aCFG. These files 
    // aren't typically necessary for regular use.
    //
	//*4*/std::string str_cent("_centrosomes");
	//     std::string filename_cent  = /*str_dir+*/str_head+ str_cent  +str_extention;
	//     std::ofstream outputcent( filename_cent );
	//     cell[0]->writeHeaderCentrosome(outputcent); // P1, P2
	///*5*/std::string str_rho("_rhoe");
	//     std::string filename_rho  = /*str_dir+*/str_head+ str_rho  +str_extention;
	//     std::ofstream outputenu( filename_rho );
	//     field->writeHeader(outputenu); //(x,y)
    //
    //------------------------------------------------------------------------------
    // Files #6~7 are storage for temporary data necessary for computing the next step.
   	/*6*/std::string str_tmp_lumen("_tmp_lumen");
	     std::string filename_tmp_lumen = /*str_dir+*/str_head+ str_tmp_lumen +str_extention;
	     std::ofstream output_tmp_lumen( filename_tmp_lumen );
	     lumen->writeHeader(output_tmp_lumen,_XMAX,_YMAX,vd,t_i,t_cell_death); //(x,y)
	/*7*/std::string str_tmp_cell("_tmp_cell");
	     std::string filename_tmp_cell  = /*str_dir+*/str_head+ str_tmp_cell  +str_extention;
	     std::ofstream output_tmp_cell( filename_tmp_cell );
	     cell_field->writeHeader(output_tmp_cell,_XMAX,_YMAX,vd,t_i,t_cell_death); //(i,j)
	/*8*/std::string str_cs("_cell_summary");
	     std::string filename_cs  = /*str_dir+*/str_head+ str_cs  +str_extention;
	     std::ofstream output_cs( filename_cs );
         cell_field->writeCellSummaryHeader(output_cs,_XMAX,_YMAX,vd,t_i,t_cell_death);
	     //cell[0]->writeParams(output_cellS,vd,t_i,t_cell_death); //(i,j)
	////////////////////////////////////////////////////////////////////////////////




	time_t t_start = std::time(nullptr);
	// Set time evolution rule_______________________________________		
	for (int t = 0; t < tstep ; t++){
        
		// update total number of cells
		tot_num_cell = tot_num_cell_tmp;


		// time evolution
		lumen->calculateNextLumenCondition(field);
		for (int m=0; m<tot_num_cell ; m++){
			cell[m]->calculateNextCellCondition(field);
		}


		// Message to confirm that the simulation is progressing
		if (t%1000==0){
			time_t now = time(NULL)-t_start;
            		cout<< "  ____t="<<t*_dt<<
			", num. of cell is "<<tot_num_cell<<
            		" ("<< now/60 <<
			" min)______________"<<endl;}
        

		// Save as data
		if( t%save_as_text_every_sec == 0){
			field->writeCondition( outputphi, /*t=*/t*_dt);
			v_organoid = field->get_organoid_volume();
			lumen->writeCondition( outputlumen,_XMAX,_YMAX, /*t=*/t*_dt, /*m=*/v_organoid);

            cell_field ->setInitialLumenCondition();
			for (int m=0; m<tot_num_cell ; m++){
				cell[m]->write2field(cell_field);
                cell[m]->writeCellSummary(output_cs ,/*t=*/t*_dt, /*m=*/m);
			}   
            cell_field->writeVisualInf( outputfile,_XMAX,_YMAX, /*t=*/t*_dt, /*m=*/tot_num_cell);
            cell_field ->setInitialLumenCondition();
        }
        
        
		// Visualization
        if( 10*t%visualize_every_sec == 0){
            v_organoid = field->get_organoid_volume();
            
			lumen->writeVisualInf( output_tmp_lumen,_XMAX,_YMAX, /*t=*/t*_dt, /*m=*/v_organoid);
            
            cell_field ->setInitialLumenCondition();
			for (int m=0; m<tot_num_cell ; m++){
				cell[m]->write2field(cell_field);
			}   
            
            cell_field->writeVisualInf( output_tmp_cell,_XMAX,_YMAX, /*t=*/t*_dt, /*m=*/tot_num_cell);
            cell_field ->setInitialLumenCondition();
		
        
        if( t%visualize_every_sec >= visualize_every_sec*0.9 ){
            FILE *gp;
            gp = popen("python3 ./make_a_snapshot.py","w");
            char* cstr = new char[filename_tmp_cell.size() + 1]; // メモリ確保

            std::char_traits<char>::copy(cstr, filename_tmp_cell.c_str(), filename_tmp_cell.size() + 1);
            fprintf(gp, cstr);
            pclose(gp);
            delete[] cstr;
            
        /*6*/std::string str_tmp_lumen("_tmp_lumen");
             std::string filename_tmp_lumen = /*str_dir+*/str_head+ str_tmp_lumen +str_extention;
             std::ofstream output_tmp_lumen( filename_tmp_lumen );
             lumen->writeHeader(output_tmp_lumen,_XMAX,_YMAX,vd,t_i,t_cell_death); //(x,y)
        /*7*/std::string str_tmp_cell("_tmp_cell");
             std::string filename_tmp_cell  = /*str_dir+*/str_head+ str_tmp_cell  +str_extention;
             std::ofstream output_tmp_cell( filename_tmp_cell );
             cell_field ->setInitialLumenCondition();
             cell_field->writeHeader(output_tmp_cell,_XMAX,_YMAX,vd,t_i,t_cell_death); //(i,j)	
            

		}    }    


		// update 
		lumen->updateLumenCondition();
		field->updateInteractionFieldCondition();
		for (int m=0; m<tot_num_cell ; m++){
			cell[m]->updateCellCondition();
		}


	
		// cell division
		for (int m=0; m<tot_num_cell ; m++){

			// check whether the cell is in the mitosis phase
			if ( cell[m]->t_cell > cell[m]->duration_interphase ){

				// check whether the cell is at the end of the mitosis phase
				//   and whether it is big enough for the division
				if( (cell[m]->t_cell  >  cell[m]->duration_interphase ) &
				    (cell[m]->v       >  cell[m]->get_max_cell_vol()-vd ) ){
					
					// centrosome position (20191017, ver2)
					for (int jikan=0; jikan<100; jikan++) {
						cell[m]->updateCentrosome(field);
						///*4*/cell[m]->writeCentrosome(outputcent,t*_dt,tot_num_cell_tmp);
					
					}
					cout<<"  final      p1=("<<cell[m]->p1_i
					<<","<<cell[m]->p1_j<<"), p2=("
					<<cell[m]->p2_i<<","<<cell[m]->p2_j<<")"<<endl;
					cout<< "  Cell "<< m <<" is divided at t="<<t*_dt<<endl;
					
					lumen->seedLumen(cell[m],/*&*/field);
					cell[tot_num_cell_tmp] = new Cell(_IMAX,_JMAX,xi,eta,rho,lumenV,t_i,t_cell_death,new_id);
                    new_id = new_id + 1;
					cell[m]->division(cell[tot_num_cell_tmp], /*&*/field, new_id);
                    new_id = new_id +1;
					cell[m]               ->initializeCentrosome();
					cell[tot_num_cell_tmp]->initializeCentrosome();

					cout<< "      (division) m="<<m<<
					", (x, y)=("<<cell[m]->xG<<","<<cell[m]->yG<<
					"), t_cell="<<cell[m]->duration_interphase<<
					", V_cell="<<cell[m]->v<<endl;
					cout<< "      (division) m="<<tot_num_cell_tmp<<
					", (x, y)=("<<cell[tot_num_cell_tmp]->xG<<","<<cell[tot_num_cell_tmp]->yG<<
					"), t_cell="<<cell[tot_num_cell_tmp]->duration_interphase<<
					", V_cell="<<cell[tot_num_cell_tmp]->v<<endl;
				
					tot_num_cell_tmp += 1;

				}
			}
		}
	}



	// output
	time_t now = time(NULL)-t_start;
        cout<< "  ____[FINAL STATE] t="<<tstep*_dt<<
	", num. of cell is "<<tot_num_cell<<
        "("<< now/60 <<
	" min)______________"<<endl;

	v_organoid = field->get_organoid_volume();
	lumen->writeCondition( outputlumen,_XMAX,_YMAX, /*t=*/tstep*_dt, /*m=*/v_organoid);



	delete[] cell;
	delete lumen;
	delete field;
	delete cell_field;
	cout << "  Please check "<< filename_lumen << endl;
	//cout << "           and "<< filename_cell << endl;
	
	cout << "end "<<endl;

}