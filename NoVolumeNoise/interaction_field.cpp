/* ////////////////////////////////////////////////////////////
   file:   interaction_field.cpp    
   brif:   Defining the 2D "interaction field" object
    	   Reference 
	    [1] Nonomura PLoS ONE 2012  
	    [2] Akiyama, et al., Phys.Biol 2018
		[3] Tanida, et al., biorxive 2023
   auther: Tanida
   date:   20191021
   update: 20200104
   ver:    1.0
/////////////////////////////////////////////////////////////*/

#include <iostream>
#include <fstream>
#include <ctime> 
#include <cstdlib>    
#include <math.h>
#include <stdexcept>

#include "cell.h"
#include "interaction_field.h"
#include "system_size.h"

using namespace std;

InteractionField::InteractionField(){
	phi      = new double[_XMAX*_YMAX]; // Valiables storing phase field val at t
	phiNext  = new double[_XMAX*_YMAX]; // Valiables storing phase field val at t+1
	chi      = new double[_XMAX*_YMAX]; // Valiables storing phase field val at t
	chiNext  = new double[_XMAX*_YMAX]; // Valiables storing phase field val at t+1
	e_nu     = new double[_XMAX*_YMAX]; // Valiables storing phase field val at t
	e_nuNext = new double[_XMAX*_YMAX];
}

InteractionField::~InteractionField(){
	delete[] phi;
	delete[] phiNext;
   	delete[] chi;
	delete[] chiNext;
	delete[] e_nu;
	delete[] e_nuNext;
}







double InteractionField::get_organoid_volume(){
	double v_organoid;
	for (int x=0; x<_XMAX ; x++){
	for (int y=0; y<_YMAX ; y++){
		v_organoid += get_phi(x,y);
	}}
	return v_organoid;
}
int InteractionField::get_index(const int i, const int j){
	if(i<0 || j<0 || i>=_XMAX || j>=_YMAX)
		throw std::out_of_range("x or y are not in range");
	return i+j*_XMAX;
}
double& InteractionField::get_phi(const int i, const int j){return phi[get_index(i,j)];}
double& InteractionField::get_phiNext(const int i, const int j){return phiNext[get_index(i,j)];}
double& InteractionField::get_chi(const int i, const int j){return chi[get_index(i,j)];}
double& InteractionField::get_chiNext(const int i, const int j){return chiNext[get_index(i,j)];}
double& InteractionField::get_e_nu(const int i, const int j){return e_nu[get_index(i,j)];}
double& InteractionField::get_e_nuNext(const int i, const int j){return e_nuNext[get_index(i,j)];}

void InteractionField::setInitialInteractionFieldCondition(){
	for (int x=0; x<_XMAX ; x++){
	for (int y=0; y<_YMAX ; y++){
		get_e_nu(x,y)=0;
		get_e_nuNext(x,y)=0;
		e_dx[x][y]=0;
		e_dy[x][y]=0;
	}}
}


void InteractionField::updateInteractionFieldCondition(){
	for (int x=0; x<_XMAX ; x++){
	for (int y=0; y<_YMAX ; y++){
		get_phi(x,y)=get_phiNext(x,y);
		get_chi(x,y)=get_chiNext(x,y);
		get_e_nu(x,y)=get_e_nuNext(x,y);
		get_phiNext(x,y)=0;
		get_chiNext(x,y)=0;
		get_e_nuNext(x,y) = 0;
		e_dx[x][y] = 0;
		e_dy[x][y] = 0;
		}}
}


void InteractionField::writeHeader(ofstream &file){
    file << "time \t" ;
	for (int x=0; x<_XMAX; x++){
	for (int y=0; y<_YMAX; y++){
		file << x <<"_"<< y << "\t";
	}}
}

void InteractionField::writeCondition(ofstream &file, double t){
	file << "\n" ;
	file << t    ;
	for (int x=0; x<_XMAX; x++){
	for (int y=0; y<_YMAX; y++){
		file <<"\t"<< get_phi(x,y);
	}}
}

void InteractionField::writeEnu(ofstream &file, double t){
	file << "\n" ;
	file << t    ;
	for (int x=0; x<_XMAX; x++){
	for (int y=0; y<_YMAX; y++){
		file <<"\t"<< get_e_nu(x,y);
	}}
}


