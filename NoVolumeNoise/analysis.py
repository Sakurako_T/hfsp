import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
from skimage.measure import regionprops
from scipy.ndimage import measurements

class CellsAndLumen:
    def __init__(self,_xmax=2000, _imax=200):
        # Simulation setup parameters
        self.xmax = _xmax
        self.imax = _imax
        self.dx = 0.02
        self.dy = 0.02
        self.dt = 0.01
        
        self.datalength = 3000
        # Initialize arrays for storing data
        self._t = np.arange(self.datalength)-3
        self.nc = np.arange(self.datalength)-3
        self.vc = np.arange(self.datalength)-3
        self.nl = np.arange(self.datalength)-3
        self.vl = np.arange(self.datalength)-3
        self.al = np.arange(self.datalength)-3
        self.vo = np.arange(self.datalength)-3
        self.vr = np.arange(self.datalength)-3

    def readParams(self,f):
        # Read simulation parameters from file
        line = f.readline()
        params= line[:-1].split('\t')
        
        xi_str = params[13]
        xi     = float(xi_str[3:])
        td_str = params[18]
        td     = float(td_str[3:])

        return td, xi

    def calculate_size(self, data_t):
        # Calculate the size (area in 2D) of cells or lumen
        u = data_t[8:]
        hu= u*u*(3-2*u)
        #print("h(u)=",sum(hu*self.dx*self.dy),", max",max(data_t[8:]),min(data_t[8:]))
        return sum(hu*self.dx*self.dy)
    
    
    def calculate_num_and_perimeter(self,data_t):
        # Calculate the number, perimeter, and total size of lumens

        # Initialize a background array and reshape the data into a 2D array
        bg = np.zeros((self.xmax + 2 * self.imax, self.xmax + 2 * self.imax))
        out = np.reshape(data_t[8:], (self.xmax, self.xmax))
        bg[self.imax:-self.imax, self.imax:-self.imax] = out

        # Binarize the output to identify lumen regions
        binarized_out = out > 0.2
        cc, num = measurements.label(binarized_out)
        regions = regionprops(cc)

        # Initialize variables to store lumen properties
        n = 0  # Number of lumens
        perimeter = 0  # Total perimeter of lumens
        area = 0
        sizes = []  # Sizes of individual lumens

        # Iterate over identified regions to calculate lumen properties
        for i in range(len(regions)):
            lumen_vol_i = regions[i].area * self.dx * self.dy
            lumen_peri_i = regions[i].perimeter * self.dx
            sizes.append(lumen_vol_i)

            # Include lumens that are larger than a specific threshold
            if lumen_vol_i > 0.785 * 2:  # Threshold for stable lumen
                n += 1
                perimeter += lumen_peri_i
                area += lumen_vol_i

        # Calculate total size of lumens
        if len(sizes) > 0:
            sizes = np.array(sizes)
            size = sizes.sum()
        else:
            size = 0

        return n, perimeter,area# size



    def process_1by1(self,n,cellfile):
        # Process each file to extract and analyze cell and lumen data

         # Retrieve the number of lines in the cell and lumen files
        s_num_lines =len(open(cellfile, "r").readlines())
        lumenfile = cellfile.replace('cell_summary','lumen') 
        num_lines =len(open(lumenfile, "r").readlines())

        # Open and read cell and lumen files
        with open(cellfile, "r") as fs, open(lumenfile,"r") as fl:#, open(cellsummaryfile, "r") as fs:
            # Open and read cell and lumen files
            td, xi = self.readParams(fs)
            print(f"   ...  (tm,xi)=({td}, {xi})")

            # Skip initial lines in the lumen file
            _ = fl.readline()
            _ = fl.readline()

            # Skip initial lines in the cell file
            _ = fs.readline()

            line_s = fs.readline()
            data_s = np.array([float(item) for item in line_s.split('\t')])
            ts = 0  # Initialize time reference


            # number of loaded lines
            nline = 0
            
            # Initialize arrays to store extracted data
            T, Nc, Vc, Nl, Vl, Al = [], [], [], [], [], []
            Vl_h = [] 
            # Process each line in the lumen file
            for line_l in fl:
                data_lu = np.array([float(item) for item in line_l.split('\t')])
                tl = float(data_lu[0])
                vh_lumen = self.calculate_size(data_lu)
                n_lumen, a_lumen, v_lumen_ = self.calculate_num_and_perimeter(data_lu)

                # Initialize cell volume and count for the current time point
                v_cells, mline = 0, 0

                # Process corresponding lines in the cell file
                while tl == ts:
                    line_s = fs.readline()
                    data_s = np.array([(item) for item in line_s.split('\t')])
                    ts = float(data_s[0])
                    n_cells = float(data_s[1])
                    v_cells += float(data_s[4])
                    mline += 1

                # Store data for analysis
                T.append(tl)
                Nc.append(mline)
                Vc.append(v_cells)
                Nl.append(n_lumen)
                Vl.append(v_lumen_)
                Vl_h.append(vh_lumen)
                Al.append(a_lumen)

                nline+=1
                # Check if the end of the file is reached
                if nline > (num_lines - 5):
                    T = np.array(T)
                    Nc= np.array(Nc)
                    Vc= np.array(Vc)
                    Nl= np.array(Nl)
                    Vl= np.array(Vl)
                    Al= np.array(Al)
                    Vl_h = np.array(Vl_h)
                    Vo= Vl_h+Vc
                    Vr= Vl_h*1./Vo
                    
                    T_=np.arange(0,self.datalength,1)
                    
                    Tc_=[n,td,xi]
                    Nc_=[n,td,xi]
                    Vc_=[n,td,xi]
                    Nl_=[n,td,xi]
                    Vl_=[n,td,xi]
                    Al_=[n,td,xi]
                    Vo_=[n,td,xi]
                    Vr_=[n,td,xi]
                    for t in range(len(T_)-3):
                        ncs = ( Nc[ T==T_[t] ])
                        vcs = ( Vc[ T==T_[t] ])
                        nls = ( Nl[ T==T_[t] ])
                        vls = ( Vl[ T==T_[t] ])
                        als = ( Al[ T==T_[t] ])
                        vos = ( Vo[ T==T_[t] ])
                        vrs = ( Vr[ T==T_[t] ])
                        if len(ncs)>0:
                            Tc_.append(T_[t])
                            Nc_.append(ncs[0])
                            Vc_.append(vcs[0])
                            Nl_.append(nls[0])
                            Vl_.append(vls[0])
                            Al_.append(als[0])
                            Vo_.append(vos[0])
                            Vr_.append(vrs[0])
                        else:
                            Tc_.append(np.nan)
                            Nc_.append(np.nan)
                            Vc_.append(np.nan)
                            Nl_.append(np.nan)
                            Vl_.append(np.nan)
                            Al_.append(np.nan)
                            Vo_.append(np.nan)
                            Vr_.append(np.nan)
                    Tc_ = np.array(Tc_)
                    Nc_ = np.array(Nc_)
                    Vc_ = np.array(Vc_)
                    Nl_ = np.array(Nl_)
                    Vl_ = np.array(Vl_)
                    Al_ = np.array(Al_)
                    Vo_ = np.array(Vo_)
                    Vr_ = np.array(Vr_)
                    
                    break
                    
        # Update class variables with the processed data
        self._t  = np.vstack([self._t , Tc_ ])
        self.nc = np.vstack([self.nc, Nc_])
        self.vc = np.vstack([self.vc, Vc_])
        self.nl = np.vstack([self.nl, Nl_])
        self.vl = np.vstack([self.vl, Vl_])
        self.al = np.vstack([self.al, Al_])
        self.vo = np.vstack([self.vo, Vo_])
        self.vr = np.vstack([self.vr, Vr_])


        print("end of this analysis")
        
    def get(self, resultlist):
        print("start the analysis")
        start = time.time()
        for i in range(0,len(resultlist)):
            self.process_1by1(i,resultlist[i])
            t = time.time() - start
            print(i,"time ",round(t/60,1)," min -----------------------")
    
def shaping_dataframe(ar, datalength):
    """
    Processes and reshapes the input array into a structured DataFrame.
    
    :param ar: Input array with data.
    :param datalength: The length of the data array.
    :return: A pandas DataFrame with processed data.
    """
    df = pd.DataFrame(ar)
    df.columns = np.arange(datalength) - 3
    df.drop(0, inplace=True)
    df.drop(columns=-3, inplace=True)
    df.rename(columns={-2: 't_d', -1: 'xi'}, inplace=True)
    df.dropna(how='all', axis=1, inplace=True)

    '''xis = np.sort(df["xi"].unique())
    tds = np.sort(df["t_d"].unique())

    df_new = pd.DataFrame()
    for xi in xis:
        for td in tds:
            df_ij = df[(df['xi'] == xi) & (df['t_d'] == td)]
            df_ij_mean = pd.DataFrame(df_ij.mean()).T
            df_new = pd.concat([df_new, df_ij_mean])'''

    return df#df_new

def analyze_data(file_pattern, datalength=3000, xmax=2000):
    """
    Analyzes the data from files matching the provided pattern.
    
    :param file_pattern: Glob pattern to match file names.
    :param datalength: Length of data in each file.
    :param xmax: Maximum x-value for data processing.
    :return: A tuple of DataFrames for each parameter.
    """
    cell_list = glob.glob(file_pattern)
    print(f"Processing {len(cell_list)} files...")

    obj = CellsAndLumen(_xmax=xmax)
    obj.get(cell_list)

    return (
        shaping_dataframe(obj._t, datalength),
        shaping_dataframe(obj.nc, datalength),
        shaping_dataframe(obj.vl, datalength),
        shaping_dataframe(obj.nl, datalength),
        shaping_dataframe(obj.al, datalength),
        shaping_dataframe(obj.vo, datalength),
        shaping_dataframe(obj.vr, datalength)
    )

def calculate_sphericity(i, df_v, df_a):
    """
    Calculates the sphericity of an organoid.
    
    :param i: Index of the DataFrame row.
    :param df_v: DataFrame containing organoid volume.
    :param df_a: DataFrame containing organoid surface area.
    :return: Sphericity value.
    """
    return 2 * np.sqrt(np.pi * df_v.iloc[i, :]) / df_a.iloc[i, :]

def calculate_lumen_index(i, df_v, df_n):
    """
    Calculates the lumen index of an organoid.
    
    :param i: Index of the DataFrame row.
    :param df_v: DataFrame containing organoid volume.
    :param df_n: DataFrame containing the number of cells.
    :return: Lumen index value.
    """
    cell_width = 1.6
    return df_v.iloc[i, :] / (df_n.iloc[i, :] * cell_width)




#========================================================
# Example usage:
# file_pattern = "./params_*_cell_summary.dat"
# dfs = analyze_data(file_pattern)
# Save DataFrames to pickle files
# for name, df in zip(['t', 'num_cell', 'area_cell', 'num_lumen', 'area_lumen', 'area_organoid', 'lumen_occupancy'], dfs):
#     df.to_pickle(f'df_{name}.pickle')
#========================================================
