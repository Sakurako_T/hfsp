# Project Title

## Description
This repository contains the implementation of the algorithm proposed in the paper entitled "Predicting organoid morphology through a phase field model: insights into cell division and lumenal pressure."

## Installation
To install the required dependencies, run:
pip install -r requirements.txt

## Directory Structure & Descriptions
- `NoVolumeNoise`: Contains simulations where no noise has been added to the volume condition of cell division.
    - `cell.cpp` : Contains calculations related to cell and lumen field values.
    - `cell.h` : Header file associated with cell.cpp.
    - `interaction_field.cpp`: Contains calculations related to interaction field values.
    - `interaction_field.h`: Header file associated with interaction_field.cpp.
    - `main.cpp`: The primary entry point and orchestrator of the simulation.
    - `make_a_gif.py`: Combines a series of images into a GIF. Before using, ensure you edit the filename of the images you want to combine.
    - `make_a_snapshot.py`: Generates snapshots of the organoid based on the simulation time intervals.
    - `params_sample.txt`: A sample parameter file. Please modify this before initiating any simulations.
    - `system_size.h`: Specifies the simulation boundary dimensions and defines parameters for time and space discretization.
    - `imgs` (directory): Directory housing the series of organoid images.
    - `movs` (directory): Repository for GIF movies, which are generated using make_a_gif.py.

- `VolumeNoise`: Contains simulations where noise has been added to the volume condition of cell division.
    - The composition of files and directories is identical to those in `NoVolumeNoise`.


## Usage

### 0. Select Scenario: Volume Noise or No Volume Noise
Before beginning, decide whether you want to run the simulation with volume noise (VolumeNoise directory) or without volume noise (NoVolumeNoise directory). Navigate to the chosen directory.

`cd VolumeNoise # or cd NoVolumeNoise`


### 1. Set parameter

Begin by preparing the parameter files for the specific parameter set you wish to investigate. Ensure you modify the filename when simulating a new parameter set to avoid overwriting existing data. The filename should start with "params_" and end with ".txt". A template detailing the format for parameter file descriptions is available. Refer to the example file named `params_sample.txt`. Each parameter is identified by the line number. For clarity in this document, we've assigned each parameter a line number corresponding to its order in the params_sample.txt file. Below is an explanation of the parameters within the template:

| line# | parameter | discription |
| ------ | ------ | ------ |
|1| tstep  | Simulation time tick. |
|2| xi     | Lumen pressure       |
|3| eta    |Cell-cell adhesion parameter|
|4| rho    ||
|5| initial lumen size  |Size of the microlumen produced during cell division|
|6| td     |Minimum time required for cell division|
|7| Vd     |Minimum volume necessary for cell division|consider adjusting this value, for example, to 10.0.|:

We advise against modifying parameters other than tstep, xi, td, and the visualization interval.We recommend setting this to a large value, e.g., 1000000. The simulation will terminate if the organoid size exceeds the simulation boundary, even if 'tstep' is set to a high number.

If you wish to alter the simulation setup, such as adjusting the system size, please modify the system_size.h file. The parameters contained within this file are detailed below:

| parameter | discription |
| ------ | ------ |
| _XMAX | Total size of the simulation system on the horizontal axis |
| _YMAX | Total size of the simulation system on the vertical axis|
| _IMAX | Horizontal size of the calculation window around each cell|
| _JMAX | Vertical size of the calculation window around each cell|
| _dx  | Horizontal length of a simulation cell for discretization　|
| _dx  | Vertical length of a simulation cell for discretization　|
| _dt  | Time duration of a single time step  |:

It's essential to adjust these parameters with caution, as they can significantly impact the simulation's results and performance. Specifically, avoid excessively increasing dt and dx. Overextending these values can cause the simulation to fall short of its stability requirements. The system size is determined based on the size of an organoid cell, which has been set to three in `cell.cpp`.


### 2. Compile
To execute the main script, use:

`g++ -g -std=c++11 exp0-08.cpp cell.cpp interaction_field.cpp -o exp0-08.out`

or use; `bash 2_compile_model.sh`

### 3. Run simulation
To run the simulation, you will be using a bash script that automates the process for you. Here are the steps:

1. Selecting Parameters
Before you run the simulation, you need to specify which parameter file the simulation should use.

Open the script 2_run_simulation.sh in a text editor of your choice. Locate the following section

```
########################################
# Please specify the parameter file name
# without the .txt extension
fname="params_****"
#fname=""
########################################
```

Replace params_**** with the name of your parameter file (without the .txt extension). For instance, if your file is named params_experiment1.txt, then you would change it to:
```
fname="params_experiment1"
```
Save and close the file.

2. Running the Script
In your Ubuntu console, navigate to the directory containing the script and run:

```
bash 2_run_simulation.sh
```

## 4. Output
Upon successful completion of the simulation, the following data files will be generated:

**1. `params_****_cell.dat`:** Contains the phase value of each cell, localized around its center of mass.
In the first row, it holds the parameter values you set. From the third row onwards, it holds the cell data at every time interval you set in `params_****.txt`.
| Column | Content |
| ------ | ------- |
| 1 | Simulation time |
| 2 | Cell's simulation label |
| 3 | X-coordinate of center of mass |
| 4 | Y-coordinate of center of mass |
| 5 | Time since the last division |
| 6 | Cell ID |
| 7 | Mother cell's ID |
| 8 onwards | Phase values (multiplied by 255) for each spatial discretization with grid size `_imax x _jmax`, which is set in `system_size.h`. |


**2. `params_****_cell_summary.dat`:** Provides information on cell volume, the time elapsed since the last division, cell id, and id of the mother cells.

**3. `params_****_lumen.dat`:** Contains the phase value of lumens.
As same as the `params_****_cell.dat`, in the first row, it holds the parameter values you set. From the third row onwards, it holds the cell data at every time interval you set in `params_****.txt`.
| Column | Content |
| ------ | ------- |
| 1 | Simulation time |
| 2 | Cell's simulation label |
| 3 | X-coordinate of center of mass |
| 4 | Y-coordinate of center of mass |
| 5 | Time since the last division |
| 6 | Cell ID |
| 7 | Mother cell's ID |
| 8 onwards | Phase values (multiplied by 255) for each spatial discretization with grid size `_XMAX x _YMAX`, which is set in `system_size.h`. |

**4. `params_****_phi.dat`:** Contains the phase value of the entire organoid.
The first row contains the header. From the third row onwards, the file provides phase values at every time interval, as specified in params_****.txt. These phase values correspond to each spatial discretization with a grid size of `_XMAX x _YMAX`.

Additionally, generated snapshots will be saved in the `/imgs` directory. These files will incorporate the parameter filename in their naming convention, making it easier to identify corresponding files.

To create a GIF movie of your simulation from the sequence of images in the `/imgs` directory, please use `for_gif_movie.sh`, which is to run `make_a_gif.py `, if you like. Please follow these steps:

1. **Edit the Script**: Modify the `3_for_gif_movie.sh` script to specify your parameter file.
   ```bash
   # 4_for_gif_movie.sh
   #!/usr/bin/bash
   ########################################
   # Specify the name of your parameter file (without the .txt extension)
   fname="params_****"
   ########################################
   echo "Preparing a GIF movie..."
   python3 ./make_a_gif.py <<EOF
   $fname
   EOF
   echo "GIF movie creation complete. Check the ./mov directory."
   ```

2. **Run the Script**: In your Ubuntu console, enter:
   ```
   bash 4_for_gif_movie.sh
   ```
   This process may take several minutes.

3. **Check the GIF**: Once completed, the GIF movie will be saved in the `/movs` directory. The filename will include the name of the parameter file for easy identification.


## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Authors
- Sakurako Tanida

## Contact
For questions, feedback, or bug reports, please contact at example@email.com.

## References
- [1] Author A, Author B. "Title of the paper". Journal, Year.
