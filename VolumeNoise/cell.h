/* ////////////////////////////////////////////////////////////
   file:   cell.h     
   brif:   Definition of 2D Cell or Lumen object
    	   Reference 
	    [1] Nonomura PLoS ONE 2012  
	    [2] Akiyama, et al., Phys.Biol 2018
		[3] Tanida, et al., biorxive 2023
   auther: Tanida
   date:   20191017
   ver:    8.1 (20210224)
   	   *add noise to the volume condition
       *use MT for random generation (get_rand)
/////////////////////////////////////////////////////////////*/

#ifndef INCLUDED_CELL_H
#define INCLUDED_CELL_H

#include "system_size.h"

class InteractionField;

//_____________________________________________

class Cell{
// 0. POSITIVE CONSTANT parameters //
	private:
		//0.1 Common parameters to Cell and Lumen
		double _tau;   // 
		double _d;     // diffusion coefficient
		double _alpha; // keep a cell volume constant (>0)
		double _beta;  // avoid overlapping (>0)
		double _gamma; // cell adhesion (_gamma > _eta)
		double _eta;   // cell adhesion

		//0.2 Parameters for Cell
		double _max_cell_vol;   
        double _vd;
        double _vd_sigma;
        double vdm;
        double _t_cell_lifetime;
		double _mean_duration_interphase;
		double _mean_duration_mitosisphase;

		double _epsilon;
		double _sigma;
		double _mu;
		double _ls;
		double _rho0;
		double _rhoe;


		//0.3 Params for Lumen
		double _initial_lumen_vol;
		double _xi;


		//0.4 DO-NOT-change Params
		int _imax;
		int _jmax;		
		double *u;
		double *uNext;
	

	public:
		Cell();
		Cell(const int, const int, const double, const double, const double, const double, const double, const double, const double, const double, const int);
		~Cell();

		// 0.5   Public Parameters for cell
        int _id;
        int _id_mother_cell;

		// 0.5.1 cell cicle
		double duration_interphase;
		double duration_mitosisphase;
		double t_cell;
        double t_cell_death;

		// 0.5.2 Cell Volume
		double v;
		double vNext;

		// 0.5.3 Center of mass
		double yG, xG;
		double diG, djG;

		// 0.5.4 Cell polarity
		double theta;

		// 0.5.5 centrosome
		double p1_i;
		double p1_j;

		double p2_i;
		double p2_j;

		



//1. Functions for Cell 	
	// 1.1 Define initial state
	void setInitialCellCondition(double, double,double);
	double F_fh(double);	   

	// 1.2 Time evolution of a Cell
	void calculateNextCellCondition(InteractionField*);
	void updateCellCondition();
	double F_du(int&, int&, int&, int&, InteractionField&); // eq.(13)
	double F_f0(int&, int&, int&, int&, InteractionField&); // eq.(14)
	double F_gint(int&, int&, int&, int&, InteractionField&); // eq.(15)	

	//1.3 Mitosis Phase
	void initializeCentrosome();
	void updateCentrosome(InteractionField*);
	double F_cortex_H(int, int);
	double F_aCFGs(int, int, InteractionField*);

	//1.4 Cell division
	void division(Cell*, InteractionField*, int);
    double F_prob_division();
	double F_chi(int, int);
	double F_uSeparation(int, int, Cell, double);





// 2.  Functions for Lumen
	//2.1 time evolution
	void setInitialLumenCondition();
	void calculateNextLumenCondition(InteractionField*);
	void updateLumenCondition();
	double F_ds(int&, int&, InteractionField&); // eq.(13)
	double F_fs(int , int , InteractionField&);
	
	//2.2 Formation of Lumen in cell 
	void seedLumen(Cell*,InteractionField*);





//3. Common Functions 
	//3.1 Other equations in Nonomura2012 & Akiyama2018
	double F_Laplacian( double, double, double, double, double); // \nabla^2
	double F_Laplacian8(double, double, double, double, double, double, double, double, double); // \nabla^2

	//3.2 small functions (get values)
	double get_max_cell_vol();
	double get_vd();
	double get_vdm();
	double get_initial_lumen_vol();
	double get_mean_duration_interphase();
	double get_mean_duration_mitosisphase();

	double get_alpha();
	double get_beta();
	double get_gamma();
	double get_eta();
	double get_epsilon();
	double get_sigma();
	double get_mu();
	double get_rho0();
	double get_rhoe();
	double get_tau();
	double get_xi();
	double get_ls();
	double get_d();
    double get_vd_sigma();

	int get_imax();
	int get_jmax();
	int get_index(const int, const int);
	double& get_u(const int, const int j);
	double& get_uNext(const int, const int j);
    
    double get_rand();
    double get_gauss_vd();

	//3.3 out put to text file
	int writeHeader(std::ofstream &, int , int, double, double, double);
	int writeCondition(std::ofstream&, int , int, double, int);
	int writeVisualInf(std::ofstream&, int , int, double, int);
	int writeHeaderCentrosome(std::ofstream&);
	int writeCentrosome(std::ofstream&, double, int);
    int write2field(Cell*);
    int writeParams(std::ofstream &,  double, double, double);
	int writeCellSummaryHeader(std::ofstream &, int , int, double, double, double);
	int writeCellSummary(std::ofstream&, int , int,  double, int);

};




#endif