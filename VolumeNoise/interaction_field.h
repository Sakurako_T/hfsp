/* ////////////////////////////////////////////////////////////
   file:   interaction_field.h    
   brif:   Defining the 2D "interaction field" object
    	   Reference 
	    [1] Nonomura PLoS ONE 2012  
	    [2] Akiyama, et al., Phys.Biol 2018
		[3] Tanida, et al., biorxive 2023
   auther: Tanida
   date:   20191021
   update: 20200104
   ver:    1.0
/////////////////////////////////////////////////////////////*/


#ifndef INCLUDED_SIMLATIONFIELD_H
#define INCLUDED_SIMLATIONFIELD_H

#include "system_size.h"


class Cell;
//_____________________________________________
class InteractionField{
	// parameters
	private:
		double* phi;     // Field variable
		double* phiNext; // Field variable	
		double* chi;     // Field variable
		double* chiNext; // Field variable
		double* e_nu;		
		double* e_nuNext;	

	public:
		InteractionField();
		~InteractionField();

	// parameters

		double e_dx[_XMAX][_YMAX];
		double e_dy[_XMAX][_YMAX];		
		//double e_nu[_XMAX][_YMAX];		
		//double e_nuNext[_XMAX][_YMAX];

		void setInitialInteractionFieldCondition();
		void updateInteractionFieldCondition();
		void updateInteractionFieldCondition_e();
		void writeHeader(std::ofstream&);
		void writeCondition(std::ofstream&, double);
		void writeEnu(std::ofstream&, double);

		
		int get_index(const int, const int);
		double get_organoid_volume();
		double& get_phi(const int, const int j);
		double& get_phiNext(const int, const int j);
		double& get_chi(const int, const int j);
		double& get_chiNext(const int, const int j);
		double& get_e_nu(const int, const int j);
		double& get_e_nuNext(const int, const int j);
		

};

#endif