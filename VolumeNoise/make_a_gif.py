from PIL import Image
import numpy as np
import glob
import time
import os
from matplotlib import cm
import matplotlib.pyplot as plt

def accumlate_imgs_in_gif(seriese_name, durationT=10):
    dir_name = './imgs/'
    img_name= dir_name + 'img_'+ seriese_name +'*.png'
    files = sorted(glob.glob(img_name))  
    images = list(map(lambda file : Image.open(file) , files))
    dir_name = './movs/'
    mov_name =dir_name + 'mov_'+ seriese_name +'.gif'
    images[0].save(mov_name, save_all = True , append_images = images[1:] , duration = durationT , loop = 0)
    
if __name__ == "__main__":
    fname=input()
    accumlate_imgs_in_gif(fname)