/* ////////////////////////////////////////////////////////////
   file:   cell.cpp    
   brif:   Definition of 2D Cell or Lumen object
    	   Reference 
	    [1] Nonomura PLoS ONE 2012  
	    [2] Akiyama, et al., Phys.Biol 2018
		[3] Tanida, et al., biorxive 2023
   auther: Tanida
   date:   20191017
   ver:    8.1 (20210224)
   	   *add noise to the volume condition
       *use MT for random generation (get_rand)
/////////////////////////////////////////////////////////////*/

#include <iostream>
#include <fstream>
#include <ctime>       
#include <cstdlib>      
#include <math.h>
#include <stdexcept>

#include "cell.h"
#include "interaction_field.h"
#include "system_size.h"

#include <random>


using namespace std;


Cell::Cell(){}
Cell::Cell(const int imax,   const int jmax, 
	   const double xi,  const double eta,
	   const double rhoe, const double vlumen,
	   const double t_i, const double t_cell_death,
       const double vd, const double vd_sigma,
       const int id){
	//---------------------------------//
	// 0. POSITIVE CONSTANT parameters //
	//---------------------------------//

	//0.1   Common parameters to Cell and Lumen
	// for Phase Field : eq(13-14) in Nonomura2012 (or eq.(2) in ref.[2])
	_tau    =1;       // (relaxation?) time 
	_d      =0.001;   // diffusion coeff. (width of membrane)
	_alpha  =1;       // keep a cell volume constant
	_beta   =1;       // volume exclusion
	_gamma  =0.01;    // cell-cell adhesion
	_eta    =eta;   // cell-cell adhesion (_eta<_gamma)

	
	//0.2   Parameters for Cell
	//0.2.1 Cell props
 	_max_cell_vol = 3;           // Maximam Cell Volume (>2)
    _vd = vd ;
    _vd_sigma = vd_sigma;
    vdm = get_gauss_vd();//_vd + 0.1*(get_rand()-0.5); // 20210122
    //cout << "vdm=" << vdm <<endl;
    _t_cell_lifetime = t_cell_death;
	_mean_duration_interphase   = t_i; // Time of INTERVAL in a cell cycle
	_mean_duration_mitosisphase = 0;  // Time of M period in a cell cycle


	//0.2.2 Params of Division
	_epsilon=0.1;             // eq.(20-21) in ref.[1]
	_sigma  =0.001;              // 
	_mu=10;                     //
	_ls=0;                     //
	_rho0 =0.01;                   //
	_rhoe =rhoe;            // cortical force generater 0~1000


	//0.3   Params for Lumen
	_initial_lumen_vol= vlumen; // Initial lumen Volume
	_xi =xi;           // Pressure


	//0.4   DO-NOT-change Params
	_imax=imax, _jmax = jmax; // system size
	u     = new double[_imax*_jmax]; // Valiables storing phase field val at t
	uNext = new double[_imax*_jmax]; // Valiables storing phase field val at t+1
    //0.5
    _id = id;
}

Cell::~Cell(){
// destructor
	delete[] u;
	delete[] uNext;
}









//////////////////////////////////////////////////////////////////////////////////////////////
//1. Functions for Cell 
//////////////////////////////////////////////////////////////////////////////////////////////

//  We consider the local area around the terget cell because the cells only 
// interact with each other in short range. We define coordinates in the cell 
// center-of-mass system and lab system as (i,j) and (x,y), respectively.

//1.1 Initial condition of Cell______________________________________________________________
void Cell::setInitialCellCondition(double xg, double yg, double initial_vol){
    
	// the center of mass (CoM)
	xG=xg; yG=yg;

	// Duration of phase and time in the Cell cicle
	double t_noise=0.2;
	duration_interphase   =_mean_duration_interphase+ t_noise*_mean_duration_interphase*(get_rand()-0.5);
	duration_mitosisphase =_mean_duration_mitosisphase + duration_interphase;
    t_cell_death = _t_cell_lifetime + t_noise*_t_cell_lifetime*(get_rand()-0.5);
    
	t_cell = _mean_duration_interphase - 15;
    //cout<<duration_mitosisphase<<", " <<t_cell <<endl;
    while (t_cell +10> duration_mitosisphase){
        t_cell -=int(10*(get_rand()-0.5));
    }
    //cout<< t_cell <<endl;
    _id_mother_cell = -1;

	// Cell polarity (Not used at present)
	theta=0.5*M_PI;

	// Cell existence (_u) and volume (v)
	double vm=0;

	double noise =0.01;
	
	for (int i=0; i<_imax ;i++){
	for (int j=0; j<_jmax ;j++){
		double di = i - (_imax-1)/2.0;
		double dj = j - (_jmax-1)/2.0;
		if ( ( (di*di+dj*dj)<((initial_vol-0.09)/M_PI/_dx/_dy) ) ){// circle
			get_u(i,j)=1+noise*get_rand()-noise*0.5;
		} 
		else {
			get_u(i,j)=0; 
		}
		vm = vm +F_fh( get_u(i,j) )*_dx*_dy; 		
	}}
	v=vm;
}

//----------------------------------------//
// Numerical Fomula

double Cell::F_fh(double u){
	// h(u) = u^2(3-2u)
	return u*u * ( 3-2*u );
}
//----------------------------------------//










// 1.2 Time evolution of a Cell__________________________________________________________________
void Cell::calculateNextCellCondition(InteractionField *field){

	InteractionField &pre_field = *field; 

	double sum_i=0, sum_j=0; // for calculation of CoM

	for (int i=1; i<_imax-1; i++){
	for (int j=1; j<_jmax-1; j++){
				
		// Coordinate Tranformation (i,j) -> (x,y)
		int x = (int)(i-_imax/2+xG);
		int y = (int)(j-_jmax/2+yG);
				
		if( (x>0) & (x<_XMAX) 
		  & (y>0) & (y<_YMAX) ){
			// time evolution of _u
			get_uNext(i,j) = get_u(i,j) + F_du(i,j,x,y,pre_field)*_dt/_tau;

			// integration of microvolume
			vNext += F_fh( get_uNext(i,j) )*_dx*_dy; 
			sum_i += F_fh( get_uNext(i,j) )*_dx*_dy*i;
			sum_j += F_fh( get_uNext(i,j) )*_dx*_dy*j;

			// excluded volume interaction (phi)
			field->get_phiNext(x,y) += F_fh( get_uNext(i,j) ); 
            
            		// ajuction(chi)20191021
            		field->get_chiNext(x,y) += F_fh( get_uNext(i,j) );

			// aCFGs
			double grad_hx = (
                F_fh(get_u(i+1,j)) - F_fh(get_u(i-1,j))+
                0.5*( F_fh( get_u(i+1,j+1) ) - F_fh( get_u(i-1,j+1) ) )+
                0.5*( F_fh( get_u(i+1,j-1) ) - F_fh( get_u(i-1,j-1) ) )   
                )*0.25/_dx;
			double grad_hy = ( 
                F_fh(get_u(i,j+1)) - F_fh(get_u(i,j-1))
                + 0.5 * (F_fh(get_u(i+1,j+1)) - F_fh(get_u(i+1,j-1)))
                + 0.5 * (F_fh(get_u(i-1,j+1)) - F_fh(get_u(i-1,j-1)))
                )*0.25/_dy;

			//if( (grad_hx!=0) || (grad_hy!=0) ){
				field->get_e_nuNext(x,y) += grad_hx * field->e_dx[x][y] 
							 +  grad_hy * field->e_dy[x][y];
                
				field->e_dx[x][y] += grad_hx ;
				field->e_dy[x][y] += grad_hy ;
		}
		else{get_uNext(i,j) = 0 ;} 
	}}
	// CoM
	diG= (1.0*sum_i/vNext)-(_imax)/2;	
	djG= (1.0*sum_j/vNext)-(_jmax)/2;	
	// time in the cell cicle
	t_cell += _dt;
}

void Cell::updateCellCondition(){
	for (int i=0; i<_imax ; i++){
	for (int j=0; j<_jmax ; j++){

		// ( i(t),j(t) ) -> ( i'(t-1),j'(t-1) ) 
		int i_before = (int)(i+diG);
		int j_before = (int)(j+djG);
		
		if( (i_before>0) & (i_before<_imax) 
		  & (j_before>0) & (j_before<_jmax)){
			get_u(i,j) = get_uNext(i_before,j_before);}
		else{get_u(i,j) = 0;}

	}}

	for (int i=0; i<_imax ; i++){
	for (int j=0; j<_jmax ; j++){
		get_uNext(i,j) = 0;
	}}

	// (i,j) -> (x,y) of CoM
	xG = (int)(diG + xG);
	yG = (int)(djG + yG);	
	//diG =0;
	//djG =0;
	v  = vNext;
	vNext = 0;
}

//----------------------------------------//
// Numerical Fomulae

double Cell::F_du(int &i, int &j, int &x, int &y, InteractionField &sys){
	// du = (dt/\tau)( D_0 \nabla^2 u + u(1-u)(u-0.5+f_0) + g_{int}
	return /*_d*F_Laplacian(  get_u(i,j),
				get_u(i+1,j),get_u(i-1,j),
				get_u(i,j+1),get_u(i,j-1) )*/
		_d*F_Laplacian8(  get_u(i,j),
				get_u(i+1,j)  ,get_u(i-1,j)  ,
				get_u(i,j+1)  ,get_u(i,j-1)  ,
				get_u(i+1,j+1),get_u(i-1,j-1),
				get_u(i+1,j-1),get_u(i-1,j+1) )
		+get_u(i,j)*(1-get_u(i,j) )*( get_u(i,j) - 0.5 + F_f0(i, j, x, y, sys) )
		+F_gint( i, j, x, y, sys ) ;
}

double Cell::F_f0(int &i, int &j, int &x, int &y, InteractionField &sys){
	// f_0(u) = \alpha_0 ( V_0 -v(u) )
    double V_target;
    double t_disappear=30;
    if(t_cell > t_cell_death){
        double x = -1.0*(t_cell - t_disappear)/t_cell_death;
        V_target = _max_cell_vol* exp(x);
    }
    else{V_target = _max_cell_vol;}
	return _alpha*( V_target - v )
		-_beta*( sys.get_phi(x,y) - F_fh(get_u(i,j)) );
}

double Cell::F_gint(int &i, int &j, int &x, int &y, InteractionField &sys){
	// g_{int} = \eta u(1-u) \nabla^2 ( \phi - h(u) ) + \gamma u(1-u) \nabla^2 h(u)
	//double psi11, psi21, psi01, psi12, psi10;
	//psi11 = sys.get_phi(x,y)   - F_fh(get_u(i,j)  ) ;
	//psi21 = sys.get_phi(x+1,y) - F_fh(get_u(i+1,j)) ;
	//psi01 = sys.get_phi(x-1,y) - F_fh(get_u(i-1,j)) ;
	//psi12 = sys.get_phi(x,y+1) - F_fh(get_u(i,j+1)) ;
	//psi10 = sys.get_phi(x,y-1) - F_fh(get_u(i,j-1)) ;
    
	double chi11, chi21, chi01, chi12, chi10, chi22, chi00, chi20, chi02;
	chi11 = sys.get_chi(x,y)   - F_fh(get_u(i,j)  ) ;
	chi21 = sys.get_chi(x+1,y) - F_fh(get_u(i+1,j)) ;
	chi01 = sys.get_chi(x-1,y) - F_fh(get_u(i-1,j)) ;
	chi12 = sys.get_chi(x,y+1) - F_fh(get_u(i,j+1)) ;
	chi10 = sys.get_chi(x,y-1) - F_fh(get_u(i,j-1)) ;
	chi22 = sys.get_chi(x+1,y+1) - F_fh(get_u(i+1,j+1)) ;
	chi00 = sys.get_chi(x-1,y-1) - F_fh(get_u(i-1,j-1)) ;
	chi20 = sys.get_chi(x+1,y-1) - F_fh(get_u(i+1,j-1)) ;
	chi02 = sys.get_chi(x-1,y+1) - F_fh(get_u(i-1,j+1)) ;

	return _eta*get_u(i,j)*(1-get_u(i,j))
		*F_Laplacian8( chi11, 
			       chi21, chi01,
			       chi12, chi10, 
			       chi22, chi00, 
                               chi20, chi02) 
		//*F_Laplacian( chi11, chi21, chi01, chi12, chi10)
		+ _gamma*get_u(i,j)*(1-get_u(i,j))
		*F_Laplacian8( F_fh(get_u(i,j)),
			      F_fh(get_u(i+1,j)),   F_fh(get_u(i-1,j)), 
			      F_fh(get_u(i,j+1)),   F_fh(get_u(i,j-1)),
			      F_fh(get_u(i+1,j+1)), F_fh(get_u(i-1,j-1)), 
			      F_fh(get_u(i+1,j-1)), F_fh(get_u(i-1,j+1)) );
}
//----------------------------------------//









//1.3 Mitosis Phase_____________________________________________________________________________

// Just after the cell get in the mitosis phase, then
void Cell::initializeCentrosome(){    
    double noise=1;
    double theta_rand1=theta+ 1*get_rand()*M_PI*2.0; //theta;//0.5*M_PI;//
    double theta_rand2=theta+ 1*get_rand()*M_PI*2.0; 
    
	p1_i=_imax/2-sin(theta_rand1)+noise*(get_rand()-0.5);
	p1_j=_jmax/2-cos(theta_rand1)+noise*(get_rand()-0.5);
	p2_i=_imax/2+sin(theta_rand1)+noise*(get_rand()-0.5);
	p2_j=_jmax/2+cos(theta_rand1)+noise*(get_rand()-0.5);
	//cout<<"initial    p1=("<<p1_i<<","<<p1_j<<"), p2=("<<p2_i<<","<<p2_j<<")"<<endl;
}



// During the mitosis phase, the centrosomes are updated.
void Cell::updateCentrosome(InteractionField *field){
	double force_p1_i=0, force_p1_j=0;
	double force_p2_i=0, force_p2_j=0;
	for (int i=0; i<_imax; i++){
	for (int j=0; j<_jmax; j++){
		int di1 = i - p1_i;
		int dj1 = j - p1_j;
		int di2 = i - p2_i;
		int dj2 = j - p2_j;

		int x = (int)(i-_imax/2+xG);
		int y = (int)(j-_jmax/2+yG);
		if( (x>0) & (x<_XMAX) 
		  & (y>0) & (y<_YMAX) ){
		if( (di1*di1+dj1*dj1) <= (di2*di2+dj2*dj2) ){
			force_p1_i += F_aCFGs(x,y,field)*F_cortex_H(i,j)*(di1)*_dx;
			force_p1_j += F_aCFGs(x,y,field)*F_cortex_H(i,j)*(dj1)*_dy;
		}
		if( (di1*di1+dj1*dj1) >= (di2*di2+dj2*dj2) ){
			force_p2_i += F_aCFGs(x,y,field)*F_cortex_H(i,j)*(di2)*_dx;
			force_p2_j += F_aCFGs(x,y,field)*F_cortex_H(i,j)*(dj2)*_dy;
		}
		}
	}}
	
	double dist_p12 = sqrt((p1_i-p2_i)*(p1_i-p2_i)+(p1_j-p2_j)*(p1_j-p2_j));
	double abs_force = sqrt(force_p1_i*force_p1_i+force_p1_j*force_p1_j);
	double noise = abs_force*0.1;
	double _dts = 1;
	p1_i += _dts/_mu*(force_p1_i- _sigma*(dist_p12-_ls)*(p1_i-p2_i)/dist_p12) ;
	p1_j += _dts/_mu*(force_p1_j- _sigma*(dist_p12-_ls)*(p1_j-p2_j)/dist_p12) ;
	p2_i += _dts/_mu*(force_p2_i+ _sigma*(dist_p12-_ls)*(p1_i-p2_i)/dist_p12) ;
	p2_j += _dts/_mu*(force_p2_j+ _sigma*(dist_p12-_ls)*(p1_j-p2_j)/dist_p12) ;
}

//----------------------------------------//
// Numerical Fomulae
//
double Cell::F_cortex_H(int i, int j){
	// H(u) = u(1-u) ref.[2]
	return get_u(i,j)*(1-get_u(i,j));
}
double Cell::F_aCFGs(int x, int y, InteractionField *field){
	// G(r,t) = rho0 - rhoe*_eta(r,t) ref.[2]
	return _rho0-_rhoe*_eta*field->get_e_nu(x,y)/6.0*2.0;
}
//----------------------------------------//









//1.4 Cell division_________________________________________________________________________

// At the end of the mitosis phase, the cell are divided into two.
void Cell::division(Cell *cell2, InteractionField *field, int id){
if(cell2){
	double xi =get_xi();
    	double eta=get_eta();
    	double rho=get_rhoe();
    	double lumenV=get_initial_lumen_vol();
	double t_m =get_xi();
    	double t_i =get_mean_duration_interphase();

    //__def. of mother cell________________
    int mother_id = _id;
	Cell mother_cell(_imax,_jmax,xi,eta,rho,lumenV,t_i,t_cell_death,_vd,get_vd_sigma(), mother_id);
	double xG_mother = xG;//(int)(diG + xG);//
	double yG_mother = yG;//(int)(djG + yG);//
	mother_cell.theta = theta;
	mother_cell.p1_i=(int)(p1_i+0.5);
	mother_cell.p1_j=(int)(p1_j+0.5);
	mother_cell.p2_i=(int)(p2_i+0.5);
	mother_cell.p2_j=(int)(p2_j+0.5);
	//cout<<"    p1=("<<mother_cell.p1_i<<","<<mother_cell.p1_j<<"), p2=("
	//	<<mother_cell.p2_i<<","<<mother_cell.p2_j<<")"<<endl;

	for (int i=0; i<_imax; i++){
	for (int j=0; j<_jmax; j++){
		// UPDATE ( i(t),j(t) ) -> ( i'(t-1),j'(t-1) ) 
		//int i_before = (int)(i+diG);
		//int j_before = (int)(j+djG);
		
		//if( (i_before>0) & (i_before<_imax) 
		//  & (j_before>0) & (j_before<_jmax)){
		//	mother_cell.get_u(i,j) = get_u(i_before,j_before);}
		//else{   mother_cell.get_u(i,j) = 0;}
		
		mother_cell.get_u(i,j) = get_u(i,j);
		get_u(i,j)=0;

		// subtract the mother cell 
		int x = (int)(i-_imax/2+xG_mother);
		int y = (int)(j-_jmax/2+yG_mother);
		field->get_phi(x,y)-= F_fh( mother_cell.get_u(i,j) ); 
		field->get_chi(x,y)-= F_fh( mother_cell.get_u(i,j) ); 
	}}


    //__Calculation of dauter cells______________________________________
    
	Cell cell1_in_mother(_imax,_jmax,xi,eta,rho,lumenV,t_i,t_cell_death,_vd,get_vd_sigma(),-2),
	     cell2_in_mother(_imax,_jmax,xi,eta,rho,lumenV,t_i,t_cell_death,_vd,get_vd_sigma(),-2);
	double sum_i_c1=0, sum_j_c1=0;
	double sum_i_c2=0, sum_j_c2=0;
	double volume_c1=0, volume_c2=0;

	double noise =0.08;
	for (int i=0; i<_imax; i++){
	for (int j=0; j<_jmax; j++){
		// Simpler division method without using centrosome
		//cell1_in_mother.F_uSeparation(i,j,mother_cell,-1);
		//cell2_in_mother.F_uSeparation(i,j,mother_cell, 1);

		// division method using centrosomes
		cell1_in_mother.get_u(i,j) = mother_cell.get_u(i,j)*   mother_cell.F_chi(i,j)*(1+noise*get_rand()-noise*0.5);
		cell2_in_mother.get_u(i,j) = mother_cell.get_u(i,j)*(1-mother_cell.F_chi(i,j))*(1+noise*get_rand()-noise*0.5);
		//cell2_in_mother.get_u(i,j) = get_u(i,j)*   F_chi(i,j) ;
		//cell1_in_mother.get_u(i,j) = get_u(i,j)*(1-F_chi(i,j));
			
		volume_c1 += F_fh( cell1_in_mother.get_u(i,j) )*_dx*_dy; 
		sum_i_c1  += F_fh( cell1_in_mother.get_u(i,j) )*_dx*_dy*i;
		sum_j_c1  += F_fh( cell1_in_mother.get_u(i,j) )*_dx*_dy*j;
	
		volume_c2 += F_fh( cell2_in_mother.get_u(i,j) )*_dx*_dy; 
		sum_i_c2  += F_fh( cell2_in_mother.get_u(i,j) )*_dx*_dy*i;
		sum_j_c2  += F_fh( cell2_in_mother.get_u(i,j) )*_dx*_dy*j;

		// subtract the mother cell 
		int x = (int)(i-_imax/2+xG_mother);
		int y = (int)(j-_jmax/2+yG_mother);
		//field->get_phi(x,y)-= F_fh( mother_cell.get_u(i,j) ); 
		//field->get_chi(x,y)-= F_fh( mother_cell.get_u(i,j) ); 
		//field->get_phi(x,y)-= F_fh( get_u(i,j) ); 
		//field->get_chi(x,y)-= F_fh( get_u(i,j) ); 

		field->get_phi(x,y) += F_fh( cell1_in_mother.get_u(i,j) );
		field->get_phi(x,y) += F_fh( cell2_in_mother.get_u(i,j) );  
		field->get_chi(x,y) += F_fh( cell1_in_mother.get_u(i,j) );
		field->get_chi(x,y) += F_fh( cell2_in_mother.get_u(i,j) ); 
        
	}}

	// gap between new and previous CoM (for coordinate transf.)
	double diG_c1 = 1.0*sum_i_c1/volume_c1 - _imax/2;
	double djG_c1 = 1.0*sum_j_c1/volume_c1 - _jmax/2;

	double diG_c2 = 1.0*sum_i_c2/volume_c2 - _imax/2;	
	double djG_c2 = 1.0*sum_j_c2/volume_c2 - _jmax/2;


    //__applying the new values_______________________________________
	for (int i=0; i<_imax ; i++){
	for (int j=0; j<_jmax ; j++){

		// (i1,j1) -> (i_m,j_m)
		int i_c1_mother = (int)(i+diG_c1);
		int j_c1_mother = (int)(j+djG_c1);

		// (i2,j2) -> (i_m,j_m)
		int i_c2_mother = (int)(i+diG_c2);
		int j_c2_mother = (int)(j+djG_c2);

		if( (i_c1_mother>0) & (i_c1_mother<_imax-1) & 
		    (j_c1_mother>0) & (j_c1_mother<_jmax-1)  ){
			/*cell1*/get_u(i,j) = cell1_in_mother.get_u(i_c1_mother,j_c1_mother);}
		else{/*cell1*/get_u(i,j) = 0;}

		if( (i_c2_mother>0) & (i_c2_mother<_imax-1) &
	 	    (j_c2_mother>0) & (j_c2_mother<_jmax-1)){
			cell2  ->get_u(i,j) = cell2_in_mother.get_u(i_c2_mother,j_c2_mother);}
		else{cell2->get_u(i,j) = 0;}
	}}

	double t_noise=0.5;
	cell2->v        = volume_c2;
	cell2->xG       = (int)(diG_c2+xG_mother);	//(i,j) -> (x,y)
	cell2->yG       = (int)(djG_c2+yG_mother);
	cell2->theta    = theta+M_PI+t_noise*(get_rand()-0.5);
	cell2->t_cell   = 0;
	cell2->duration_interphase   =_mean_duration_interphase+t_noise*_mean_duration_interphase*(get_rand()-0.5);
	cell2->duration_mitosisphase =_mean_duration_mitosisphase + cell2->duration_interphase;
    cell2->t_cell_death = _t_cell_lifetime + t_noise*_t_cell_lifetime*(get_rand()-0.5);
    cell2->_id_mother_cell = mother_id;
    cell2->vdm = get_gauss_vd();
		
	/*cell1*/v      = volume_c1;	
	/*cell1*/xG     = (int)(diG_c1+xG_mother);	//(i,j) -> (x,y)
	/*cell1*/yG     = (int)(djG_c1+yG_mother);
	/*cell1*/theta  = theta+M_PI+t_noise*(get_rand()-0.5);
	/*cell1*/t_cell = 0;
	/*cell1*/duration_interphase   =_mean_duration_interphase+t_noise*_mean_duration_interphase*(get_rand()-0.5);
	/*cell1*/duration_mitosisphase =_mean_duration_mitosisphase + duration_interphase;
    /*cell1*/t_cell_death = _t_cell_lifetime + t_noise*_t_cell_lifetime*(get_rand()-0.5);
    /*cell1*/_id = id;
    /*cell1*/_id_mother_cell = mother_id;
    /*cell1*/vdm = get_gauss_vd();
    
    //cout <<"   ids:"<< _id <<", "<<cell2->_id<<endl;
    //cout <<"   mother ids:"<< _id_mother_cell <<", "<<cell2->_id_mother_cell<<endl;

	
	}
}


double Cell::F_prob_division(){
    double dv = v-get_vd();
    double del= 0.01;
    double prob =1.0/(1+exp( -dv/del ));
    return prob;
}

// When division, a drop of extracellular fluid is formed between the new 2 cell.
// See also 2.2.

//----------------------------------------//
// Numerical Fomulae


double Cell::F_chi(int i, int j){
	// X(r) = 1/2 * ( 1 + tanh( g(r)/epsilon ) )
	double d_p12 = sqrt((p1_i-p2_i)*(p1_i-p2_i)+(p1_j-p2_j)*(p1_j-p2_j));
	double contractile_ring = (p1_i-p2_i)/d_p12* (i- 0.5*(p1_i+p2_i ) )
				+ (p1_j-p2_j)/d_p12* (j- 0.5*(p1_j+p2_j ) );
	return 0.5*(1+tanh( contractile_ring/_epsilon ) );
}

double Cell::F_uSeparation(int i, int j, Cell mother_cell, double pm){
	// X(r) = 1/2 * ( 1 + tanh( vec(theta) * (r-r_m) /epsilon ) )
	get_u(i,j) = mother_cell.get_u(i,j)*0.5*
		(1+pm*tanh( ( (_imax*0.5-i)*cos(mother_cell.theta)+
 			   (_jmax*0.5-j)*sin(mother_cell.theta) )/_epsilon ) );
    return 0;
}
//----------------------------------------//











//////////////////////////////////////////////////////////////////////////////////////////////
// 2.  Functions for Lumen 
//////////////////////////////////////////////////////////////////////////////////////////////

// The coordinate of Lumen is same as the Lab system (x,y).

// 2.1 Initial condition of Lumen
void Cell::setInitialLumenCondition(){
	// the center of mass (CoM)
	xG=_XMAX/2; yG=_YMAX/2;

	// Duration of phase and time in the Cell cicle
	duration_interphase   =0;
	duration_mitosisphase =0;
	t_cell                =0;

	// Cell polarity (Not used at present)
	theta=0;

	// Cell existence (_u) and volume (v)
	for (int i=0; i<_imax ;i++){
	for (int j=0; j<_jmax ;j++){
		get_u(i,j)=0;	
	}}
	v=0;

	// no centrosome
	p1_i=0 ;p1_j=0;
	p2_i=0 ;p2_j=0;
}








// 2.2 time evolution rule____________________________________________________________________
void Cell::calculateNextLumenCondition(InteractionField *field){

	InteractionField &pre_field = *field;	

	for (int x=1; x<_XMAX-1; x++){
	for (int y=1; y<_YMAX-1; y++){
		// Time evolution of _u 
		get_uNext(x,y) = get_u(x,y) + F_ds(x,y,pre_field)*_dt;
		// Volume exclusion interaction
		field->get_phiNext(x,y) += F_fh( get_uNext(x,y) );
	}}
}

void Cell::updateLumenCondition(){
	for (int x=0; x<_XMAX ; x++){
	for (int y=0; y<_YMAX ; y++){
		get_u(x,y) = get_uNext(x,y);
		get_uNext(x,y) = 0;
	}}
}

//----------------------------------------//
// Numerical Fomulae
//
double Cell::F_ds(int &x, int &y, InteractionField &sys){
	// ds = (dt/\tau)( D_0 \nabla^2 s + s(1-s)(s-0.5+f_s), 
	return /*_d*F_Laplacian(  get_u(x,y),
			        get_u(x+1,y),get_u(x-1,y),
			        get_u(x,y+1),get_u(x,y-1) )*/
		_d*F_Laplacian8(  get_u(x,y),
			        get_u(x+1,y),  get_u(x-1,y),
			        get_u(x,y+1),  get_u(x,y-1) ,
			        get_u(x+1,y+1),get_u(x-1,y-1),
			        get_u(x+1,y-1),get_u(x-1,y+1) )
		+get_u(x,y)*(1-get_u(x,y) )*( get_u(x,y) - 0.5 + F_fs( x, y, sys) );
}
double Cell::F_fs(int x, int y, InteractionField &sys){
	// fs( u(r) ) = xi - beta*phi
	return _xi-_beta*( sys.get_chi(x,y));// - F_fh(get_u(x,y)) );
}
//----------------------------------------//









//2.3 Formation of Lumen in cell division_______________________________________________ 
void Cell::seedLumen(Cell *mother_cell, InteractionField *field){
	double Pi_1 = mother_cell->p1_i;
	double Pj_1 = mother_cell->p1_j;
	double Pi_2 = mother_cell->p2_i;
	double Pj_2 = mother_cell->p2_j;
	
	int XG = (int)( mother_cell->xG);//mother_cell->diG +
	int YG = (int)( mother_cell->yG);//mother_cell->djG +

	//(i,j)->(x,y)
	int imax=mother_cell->_imax;
	int jmax=mother_cell->_jmax;
	double cx =(Pi_1+Pi_2)/2-(imax)/2+XG;
	double cy =(Pj_1+Pj_2)/2-(jmax)/2+YG;
	for (int i=1; i<imax-1; i++){
	for (int j=1; j<jmax-1; j++){
				
		// (i,j) -> (x,y)
		int x = (int)(i-imax/2+XG);
		int y = (int)(j-jmax/2+YG);
		
				
		if( (x>0) & (x<_XMAX) 
		  & (y>0) & (y<_YMAX) ){
			int Dx = (int)(x - cx);
			int Dy = (int)(y - cy);
			if ( ( (Dx*Dx+Dy*Dy)<(_initial_lumen_vol/M_PI/_dx/_dy) ) ){
				get_u(x,y) = 1;//mother_cell->get_u(i,j);
				field->get_phi(x,y) = F_fh(1);
                		field->get_chi(x,y) = F_fh(0);
				mother_cell->get_u(i,j) = 0;
			}
		} 
	}}
}













/////////////////////////////////////////////////////////////////////////////////////////
//3. Common Functions 
/////////////////////////////////////////////////////////////////////////////////////////

//3.1 Other equations in Nonomura2012 & Akiyama2018
// \nabla^2 
double Cell::F_Laplacian(double u11,double u21, double u01, double u12, double u10){
	return ( (u21-u11)/_dx - (u11-u01)/_dx )/_dx
		+ ( (u12-u11)/_dy - (u11-u10)/_dy )/_dy;
}

double Cell::F_Laplacian8(double u11,double u21, double u01, double u12, double u10,
				     double u22, double u00, double u20, double u02){
	return (
	  (u10 - 2.0*u11 + u12)/(_dx*_dx)
	  +(u01 - 2.0*u11 + u21)/(_dy*_dy)
	  +( 0.5*( u00 + u02 + u20 + u22 ) - 2.0*u11 )/(_dx*_dy) )*0.5;
}








//3.2 small functions (get values)______________________________________________________
double Cell::get_max_cell_vol()              {return _max_cell_vol;}
double Cell::get_vd()                        {return _vd;}
double Cell::get_vdm()                       {return vdm;}
double Cell::get_initial_lumen_vol()         {return _initial_lumen_vol;}
double Cell::get_mean_duration_interphase()  {return _mean_duration_interphase;}
double Cell::get_mean_duration_mitosisphase(){return _mean_duration_mitosisphase;}

double Cell::get_alpha()  {return _alpha;}
double Cell::get_beta()   {return _beta;}
double Cell::get_gamma()  {return _gamma;}
double Cell::get_eta()    {return _eta;}
double Cell::get_epsilon(){return _epsilon;}
double Cell::get_sigma()  {return _sigma;}
double Cell::get_mu()     {return _mu;}
double Cell::get_rho0()   {return _rho0;}
double Cell::get_rhoe()   {return _rhoe;}
double Cell::get_tau()    {return _tau;}
double Cell::get_xi()     {return _xi;}
double Cell::get_ls()     {return _ls;}
double Cell::get_d()      {return _d;}
double Cell::get_vd_sigma()      {return _vd_sigma;}


int Cell::get_imax(){return _imax;}
int Cell::get_jmax(){return _jmax;}
int Cell::get_index(const int i, const int j){
	if(i<0 || j<0 || i>=_imax || j>=_jmax)
		throw std::out_of_range("i or j are not in range");
	return i+j*_imax;
}
double& Cell::get_u(const int i, const int j){return u[get_index(i,j)];}
double& Cell::get_uNext(const int i, const int j){return uNext[get_index(i,j)];}

double Cell::get_rand(){
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> genrand(0.0, 1.0);
    return genrand(mt);
}

double Cell::get_gauss_vd(){
    std::random_device rd;
    std::mt19937 mt(rd());
    std::normal_distribution<> dist(get_vd(), get_vd_sigma() );
    return dist(mt);
}






//3.3 Output
int Cell::writeHeader(ofstream &file, int imax, int jmax, double vd, double tc, double t_cell_death){
    writeParams(file, vd, tc, t_cell_death);
    return 0;
}


int Cell::writeCondition(ofstream& file, int imax, int jmax,double t, int m){
	file << "\n" ;
	file << t    << "\t";
	file << m    << "\t";
	file << xG   << "\t";
	file << yG   << "\t";
	file << v << "\t";
	file << t_cell << "\t";
	file << _id << "\t";
	file << _id_mother_cell;
	for (int i=0; i<imax; i++){
	for (int j=0; j<jmax; j++){
		file  << "\t"<< float(get_u(i,j) );
	}}
    return 0;
}

int Cell::writeVisualInf(ofstream& file, int imax, int jmax,double t, int m){
	file << "\n" ;
	file << t    << "\t";
	file << m    << "\t";
	file << xG   << "\t";
	file << yG   << "\t";
	file << v << "\t";
	file << t_cell << "\t";
	file << _id << "\t";
	file << _id_mother_cell;
	for (int i=0; i<imax; i++){
	for (int j=0; j<jmax; j++){
		file  << "\t"<< int(get_u(i,j)*255 );
	}}
    return 0;
}


int Cell::writeCentrosome(ofstream& file, double t, int m){
	file << "\n" ;
	file << t    << "\t";
	file << m   << "\t";
	file << xG   << "\t";
	file << yG   << "\t";
	file << p1_i << "\t";
	file << p1_j << "\t";
	file << p2_i << "\t";
	file << p2_j <<endl;
    return 0;
}

int Cell::writeHeaderCentrosome(ofstream& file){
	file << "time\t";
	file << "divisionNum\t" ;
	file << "xG\t";
	file << "yG\t";
	file << "p1_i\t";
	file << "p1_j\t";
	file << "p2_i\t";
	file << "p2_j"<<endl;
	return 0;
}

int Cell::write2field(Cell *field){
    //cout << "(XG, YG)= ("<<xG<<", " <<yG<<")"<<endl;
	for (int i=0; i<_imax; i++){
	for (int j=0; j<_jmax; j++){
		// subtract the mother cell 
		int x = (int)(i-_imax/2+xG);
		int y = (int)(j-_jmax/2+yG);
        if( (x>0) & (x<_XMAX) 
		  & (y>0) & (y<_YMAX) ){field->get_u(x,y) += get_u(i,j); }
	}}
    return 0;
}




int Cell::writeParams(ofstream &file,  double vd, double tc, double t_cell_death){
	file << "MaxCellVol="          << get_max_cell_vol()          << "\t";
	file << "InitialLumenVol="     << get_initial_lumen_vol()         << "\t";
	file << "Mean_T_interphase="   << get_mean_duration_interphase()   << "\t";
	file << "Mean_T_mitosisphase=" << get_mean_duration_mitosisphase() << "\t";
	file << "alpha="   << get_alpha()   << "\t";
	file << "beta="    << get_beta()    << "\t";
	file << "gamma="   << get_gamma()   << "\t";
	file << "eta="     << get_eta()     << "\t";
	file << "epsilon=" << get_epsilon() << "\t";
	file << "sigma="   << get_sigma()   << "\t";
	file << "mu="      << get_mu()      << "\t";
	file << "rho0="    << get_rho0()    << "\t";
	file << "rhoe="    << get_rhoe()    << "\t";
	file << "xi="      << get_xi()      << "\t";
	file << "ls="      << get_ls()      << "\t";
	file << "D="       << get_d()       << "\t";
	file << "tau="     << get_tau()     <<"\t";
	file << "Vd="      << vd       <<"\t";
	file << "Tc="      << tc       <<"\t";
	file << "dVd="      << get_vd_sigma()      <<"\t";
	file << "T_cell_death="<< t_cell_death       << "\n";
    return 0;
}


int Cell::writeCellSummaryHeader(ofstream &file, int imax, int jmax, double vd, double tc, double t_cell_death){

    writeParams(file, vd, tc, t_cell_death);
    
	file << "time\t";
	file << "m\t";
	file << "xG\t";
	file << "yG\t";
	file << "v\t";//"p1 x\t";
	file << "t_cell\t";//"p1 y\t";
	file << "id\t";
	file << "mother id"<<endl;
    return 0;
}

int Cell::writeCellSummary(ofstream& file, int imax, int jmax, double t, int m){
	//file << "\n" ;
	file << t    << "\t";
	file << m   << "\t";
	file << xG   << "\t";
	file << yG   << "\t";
	file << v    << "\t";
	file << t_cell<< "\t";
	file << _id << "\t";
	file << _id_mother_cell;
	//for (int i=0; i<imax; i++){
	//for (int j=0; j<jmax; j++){
	//	file  << "\t"<< int(get_u(i,j)*255 );
	//}}
    file << "\n" ;
    return 0;
}

//_end of the code_____________________________________________________________________














