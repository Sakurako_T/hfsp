// system_size.h
/* ////////////////////////////////////////////////////////////
	system size
/////////////////////////////////////////////////////////////*/

#ifndef INCLUDED
#define INCLUDED

#define _XMAX 256
#define _YMAX 256

#define _IMAX 100
#define _JMAX 100

#define _dx 0.05
#define _dy 0.05

#define _dt 0.01


#endif
